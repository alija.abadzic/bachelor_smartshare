/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.smartsocietyproject.scenario2.helper;

/**
 *
 * @author Svetoslav Videnov <s.videnov@dsg.tuwien.ac.at>
 */
public class JsonPeer {
    private String pw;
    private String name;
    private String channelType;
    private String channel;
    private String role;

    private String points;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public String getChannelType() {
        return channelType;
    }

    public String getChannel() {
        return channel;
    }

    public String getRole() {
        return role;
    }
    
}
