Scenario5 is implemented as part of my bachelor thesis: *A Platform for Decentralized Collaborative Redistribution of Shared Vehicle Fleets in a Smart City*

`WebApp.java` and `Scenario5.java` need to be started separetely.


**Compilation**

*windows*: ``` gradlew.bat build ```
   
*unix*: ```./gradlew build```

