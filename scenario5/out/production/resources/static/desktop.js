var pathname = window.location.pathname;
if(pathname === "/home" && !localStorage.getItem("peer")){
    window.location.href = "/";
}
else if (pathname !== "/home" && localStorage.getItem("peer") ){
    window.location.href = "/home";
}
else {}
var mymap;


$(document).ready(function(){
    var pathname = window.location.pathname;
    if(pathname === "/home" ){
        $('#greeting').html("Hello <u>" + localStorage.getItem("peer") + "</u>!");
        var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        document.getElementById("datetime").value = (new Date(Date.now() - tzoffset)).toISOString().slice(0, 16);
    }
    addListeners();


});

function loadMap(address) {
    $('#map').show()
    mymap = L.map('map').setView([48.20849, 16.37208], 12);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 17,
        minZoom: 12,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);
    var top = window.scrollY + 200;
    console.log(window.scrollY)
    $('#map').css({ top: top });


    L.esri.Geocoding.geocode().address(address).city("Vienna").run(function(err, results, response){
        console.log(results.results[0].latlng)
        var latlng = results.results[0].latlng
        L.circle([latlng.lat,latlng.lng], 1, {
            fillColor: 'red',
            color: 'red',
            fillOpacity: 1,
        }).addTo(mymap);
    });
}


function addListeners(){

    $('#close_map').click(function () {
        $('#map').toggle()
        mymap.off();
        mymap.remove();
    })



    function getTasks() {
        $("#tasks").find("tr:gt(0)").remove();
        var obj = {};
        obj.peer = localStorage.getItem("peer");
        var formData = JSON.stringify(obj);
        var table = $('#tasks');

        $.ajax({
            type: "POST",
            url: "http://localhost:9696/request/tasks",
            data: formData,
            contentType : "application/json",
            success: function(data){
                var tasks = JSON.parse(data);
                tasks = tasks.tasks;
                console.log(tasks)
                for (var i = 0; i < tasks.length; i++) {
                    var task = tasks[i];
                    var firstColumn = "Task";
                    var request = JSON.parse(task.request);
                    var showMap = false;
                    var map = "<div id=\"show_map\" data-address=" + request.address + " class=\"btn\" style=\"display: inline\">Show on map</div>";
                    // compare dates
                    var curr = moment();
                    var tasktime = moment(request.datetime,'DD.MM.YYYY HH:mm').toDate();
                    var duration = moment.duration(curr.diff(tasktime));
                    var minutes = Math.round(duration.asMinutes());
                    if(task.status === "finished" && task.peer === localStorage.getItem("peer") && minutes<=60){
                        showMap = true;
                    }
                    if (task.peer === localStorage.getItem("peer")) firstColumn = "Request";
                    var appender = "<tr>\n" +
                        "        <td class='table_row' style=\"width: 139px;\">" + firstColumn +"</td>\n" +
                        "        <td class='table_row' style=\"width: 139px;\">" + request.address + "</td>\n" +
                        "        <td class='table_row' style=\"width: 140px;\">" + request.datetime + "</td>\n" +
                        "        <td class='table_row' style=\"width: 140.224px;\">" + task.status + "</td>\n"
                    if(showMap){
                        appender += "        <td style=\"width: 140.224px; text-align: center\">" + map  + "</td>\n" +
                            "    </tr>"
                    }
                    else{
                        appender += "        <td style=\"width: 140.224px; text-align: center\"></td>\n" +
                            "    </tr>"
                    }

                    table.append(appender);
                }
                $('#show_map').click(function () {
                    loadMap($(this).data('address'))
                })
            },
            error: function(){
                $alert.show().text('No tasks from found!').delay(3000).fadeOut();
            }

        });
    }

    $('#show_tasks').click(function () {

        if(! ($('#tasks').is(':visible')) ){
            getTasks()
        }

        $('#tasks').toggle();


    })

    $alert = $('#alert');

    $('#register_button').click(function () {
        var requestForm = $('#register_form').serializeArray();
        var requestObject = {};
        $.each(requestForm,
            function(i, v) {
                requestObject[v.name] = v.value;
            });

        for(var key in requestObject) {
            if (requestObject[key] === "") {
                $alert.show().text('Please fill all fields!').delay(3000).fadeOut();;
                return;
            }
        }
        console.log(requestObject)
        if(requestObject.pw !== requestObject.repeat_pw) {
            $alert.show().text('Password needs to match!').delay(3000).fadeOut();;
            return;
        }
        requestObject.conversation = "smartShare";
        requestObject.type = "register";
        requestObject.subtype = "question";
        requestObject.sender = requestObject.name;

        requestObject.content = "{ \"name\": \"" + requestObject.name + "\", \"channel\": \"" + requestObject.channel + "\" , \"password\": \"" + requestObject.pw + "\"  }"
        delete requestObject.name;
        delete requestObject.pw;
        delete requestObject.repeat_pw;
        delete requestObject.channel;
        var formData = JSON.stringify(requestObject);


        $.ajax({
            type: "POST",
            url: "http://localhost:9696/request",
            data: formData,
            contentType : "application/json",
            success: function(data){
                $alert.show().text('User registered!').delay(3000).fadeOut();

            }

        });
    });




    $('#request_button').click(function () {
        var form = new FormData(document.getElementById("request_form"))
        var formObject = {};
        form.forEach(function(value, key){
            formObject[key] = value;
        });
        var requestObject = {};
        if(formObject.datetime === "" || formObject.address === ""){
            $alert.show().text('Please fill all fields!').delay(3000).fadeOut();;
            return;
        }
        requestObject.conversation = "smartShare";
        requestObject.type = "request";
        requestObject.subtype = "question";
        requestObject.sender = localStorage.getItem("peer");

        formObject.datetime = moment(formObject.datetime).format('DD.MM.YYYY HH:mm');

        //var datetime = moment(requestObject.datetime).format('DD.MM.YYYY HH:mm');
        //requestObject.content = "address: " + requestObject.address + ",  datetime: " + datetime;

        requestObject.content = {};
        requestObject.content.address = formObject.address;
        requestObject.content.datetime = formObject.datetime;
        requestObject.content = JSON.stringify(requestObject.content);
        delete requestObject.datetime;
        delete requestObject.address;
        var formData = JSON.stringify(requestObject);

        $.ajax({
            type: "POST",
            url: "http://localhost:9696/request",
            data: formData,
            contentType : "application/json",
            success: function(){
                $alert.show().text('Request sent!').delay(2000).fadeOut();
                setTimeout(function(){
                    location = ''
                },3000)
            },
            error: function(){
                $alert.show().text('Check your points!').delay(3000).fadeOut();
            }

        });
    });

    $('#login_button').click(function () {
        var requestForm = $('#login_form').serializeArray();
        var requestObject = {};
        $.each(requestForm,
            function(i, v) {
                requestObject[v.name] = v.value;
            });

        for(var key in requestObject) {
            if (requestObject[key] === "") {
                alert("Please fill all fields!");
                return;
            }
        }
        console.log(requestObject)
        requestObject.conversation = "smartShare";
        requestObject.type = "login";
        requestObject.subtype = "question";
        requestObject.sender = requestObject.name;

        requestObject.content = "{ \"name\": \"" + requestObject.name + "\",  \"password\": \"" + requestObject.pw + "\"  }";
        var peer = requestObject.name;
        delete requestObject.name;
        delete requestObject.pw;
        var formData = JSON.stringify(requestObject);



        $.ajax({
            type: "POST",
            url: "http://localhost:9696/request",
            data: formData,
            contentType : "application/json",
            success: function(data){
                console.log(data);
                localStorage.peer = peer;
                window.location.href = "home";
            },
            error: function(){
                $alert.show().text('Login failed!').delay(3000).fadeOut();
            }

        });
    });

    $('#logout').click(function () {
        localStorage.removeItem("peer");
        window.location = "/";
    })
}



