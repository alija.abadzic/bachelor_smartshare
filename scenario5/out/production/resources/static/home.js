loadData();

$(document).ready(function(){


});



function loadData() {
    $alert = $('#alert');
    var obj = {};
    obj.peer = localStorage.getItem("peer");
    var formData = JSON.stringify(obj);


    $.ajax({
        type: "POST",
        url: "http://localhost:9696/request/points",
        data: formData,
        contentType : "application/json",
        success: function(data){
            console.log(data);
            var object = JSON.parse(data);
            $('#points').text("Current number of points: " + object.points);
        },
        error: function(){
            $alert.show().text('History not found!').delay(3000).fadeOut();
        }

    });
}
