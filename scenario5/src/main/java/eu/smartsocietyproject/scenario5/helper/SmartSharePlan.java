/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.smartsocietyproject.scenario5.helper;

import eu.smartsocietyproject.pf.ApplicationBasedCollective;
import eu.smartsocietyproject.pf.Member;
import eu.smartsocietyproject.pf.Plan;
import eu.smartsocietyproject.scenario5.SmartShareTaskRequest;


public class SmartSharePlan extends Plan {
    private ApplicationBasedCollective humans;
    private SmartShareTaskRequest request;

    public SmartSharePlan(ApplicationBasedCollective humans,
                          SmartShareTaskRequest request) {
        this.humans = humans;
        this.request = request;
    }


    public ApplicationBasedCollective getHumans() {
        return humans;
    }


    public SmartShareTaskRequest getRequest() {
        return request;
    }
}
