package eu.smartsocietyproject.scenario5.helper;

import at.ac.tuwien.dsg.smartcom.model.Message;
import eu.smartsocietyproject.peermanager.PeerManagerException;
import eu.smartsocietyproject.pf.AttributeType;
import eu.smartsocietyproject.pf.helper.InternalPeerManager;
import eu.smartsocietyproject.pf.helper.PeerIntermediary;


public final class SmartShareUtil {

    public static final String FINISHED = "finished";
    public static final String FAILED = "failed";
    public static final String PENDING = "pending";


    public static void assignPointToPeer(String id, InternalPeerManager pm) {
        PeerIntermediary peerIntermediary;
        try {
            peerIntermediary = pm.readPeerById(id);
            Integer points = (Integer) peerIntermediary.getAttribute("points", AttributeType.INTEGER).getValue();
            pm.updatePeer(peerIntermediary,points+1);
        } catch (PeerManagerException e) {
            e.printStackTrace();
        }
    }


    public static boolean deductPointToPeer(Message msg, InternalPeerManager pm) {
        PeerIntermediary peerIntermediary;
        try {
            peerIntermediary = pm.readPeerById(msg.getSenderId().getId());
            Integer points = (Integer) peerIntermediary.getAttribute("points", AttributeType.INTEGER).getValue();
            if(points < 1){
                return false;
            }
            pm.updatePeer(peerIntermediary,points-1);

        } catch (PeerManagerException e) {
            e.printStackTrace();
        }
        return true;
    }
}
