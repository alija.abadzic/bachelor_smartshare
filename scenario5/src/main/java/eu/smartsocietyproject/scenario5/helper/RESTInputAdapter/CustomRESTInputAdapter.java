package eu.smartsocietyproject.scenario5.helper.RESTInputAdapter;

import at.ac.tuwien.dsg.smartcom.adapter.InputPushAdapter;
import at.ac.tuwien.dsg.smartcom.adapter.util.TaskScheduler;
import at.ac.tuwien.dsg.smartcom.adapters.rest.JsonMessageDTO;
import at.ac.tuwien.dsg.smartcom.adapters.rest.ObjectMapperProvider;
import at.ac.tuwien.dsg.smartcom.broker.InputPublisher;
import at.ac.tuwien.dsg.smartcom.model.Identifier;
import at.ac.tuwien.dsg.smartcom.model.Message;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.util.JSON;
import eu.smartsocietyproject.peermanager.PeerManagerException;
import eu.smartsocietyproject.pf.AttributeType;
import eu.smartsocietyproject.pf.SmartSocietyApplicationContext;
import eu.smartsocietyproject.pf.helper.InternalPeerManager;
import eu.smartsocietyproject.pf.helper.PeerIntermediary;
import eu.smartsocietyproject.scenario2.helper.JsonPeer;
import eu.smartsocietyproject.scenario5.Scenario5;
import eu.smartsocietyproject.scenario5.handler.SmartShareExecutionHandler;
import eu.smartsocietyproject.scenario5.helper.SmartShareUtil;
import org.bson.BSON;
import org.bson.Document;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@Path("/")
@Singleton
public class CustomRESTInputAdapter extends InputPushAdapter {
    private static final Logger log = LoggerFactory.getLogger(CustomRESTInputAdapter.class);

    private HttpServer server;
    private final URI serverURI;

    private static InternalPeerManager pm;

    /**
     * @deprecated only used by frameworks
     */
    @Inject
    public CustomRESTInputAdapter(InputPublisher publisher, TaskScheduler scheduler) {
        this(8080, "dummy");
        setInputPublisher(publisher);
        setScheduler(scheduler);
    }

    @Deprecated
    public CustomRESTInputAdapter(int port, String serverURIPostfix) {
        this.serverURI = URI.create("http://localhost:"+port+"/"+serverURIPostfix);
    }

    public CustomRESTInputAdapter(int port, String serverURIPostfix,SmartSocietyApplicationContext context) {
        this.serverURI = URI.create("http://localhost:"+port+"/"+serverURIPostfix);
        pm = (InternalPeerManager) context.getPeerManager();
    }

    @Override
    protected void cleanUp() {
        server.shutdown();
    }

    @Override
    public void init() {
        server = GrizzlyHttpServerFactory.createHttpServer(serverURI, new CustomRESTInputAdapter.RESTApplication());
        try {
            server.start();
        } catch (IOException e) {
            log.error("Could not initialize RESTInputAdapter", e);
        }
    }

    private class RESTApplication extends ResourceConfig {
        private RESTApplication() {
            register(CustomRESTInputAdapter.class);
            register(ObjectMapperProvider.class);
            register(JacksonFeature.class);
            register(CorsFilter.class);
            register(new AbstractBinder() {
                @Override
                protected void configure() {
                    bind(inputPublisher).to(InputPublisher.class);
                    bind(scheduler).to(TaskScheduler.class);
                }
            });
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response message(JsonMessageDTO message)  {
        if (message == null) {
            throw new WebApplicationException();
        }

        if (message.getType().equals("login")){
            Integer points = Scenario5.loginPeer(message.createMessage().getContent());
            return points != null ? Response.ok(points.toString()).build() : Response.status(Response.Status.BAD_REQUEST).build();
        }

        Message msg = message.createMessage();
        boolean valid = SmartShareUtil.deductPointToPeer(msg,pm);
        if (!valid){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        log.error("komtt es hier");
        publishMessage(message.createMessage());

        return Response.status(Response.Status.OK).build();
    }

    @POST
    @Path("/points")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPoints(org.json.simple.JSONObject message) {
        if (message == null) {
            throw new WebApplicationException();
        }
        Integer points;
        PeerIntermediary peerIntermediary;
        try {
            peerIntermediary = pm.readPeerById(message.get("peer").toString());
            points = (Integer) peerIntermediary.getAttribute("points",AttributeType.INTEGER).getValue();
        } catch (PeerManagerException e) {
            log.error(e.getMessage());
            return null;
        }
        //log.error(points.toString());
        return Response.ok(new JSONObject().put("points",points).toString()).build();

    }

    @GET
    @Path("/accept/{peer}/{param}/{taskID}")
    @Produces(MediaType.TEXT_PLAIN)
    public String accept(@PathParam("peer") String peer, @PathParam("param") String taskId, @PathParam("taskID") String taskID) {

        //log.error(points.toString());
        String status = pm.getTaskStatus(taskID);
        String msg;
        switch (status) {
            case SmartShareUtil.FAILED:
                msg = "Community time expired. Request failed!";
                break;
            case SmartShareUtil.FINISHED:
                msg = "Request already accepted by other peer!";
                break;
            default:
                msg = "Thank you for the response!";
                break;
        }
        publishMessage(new Message.MessageBuilder().setConversationId(taskId).setSenderId(Identifier.peer(peer)).create());
        return msg;

    }

    @POST
    @Path("/tasks")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTasks(org.json.simple.JSONObject message) {
        if (message == null) {
            throw new WebApplicationException();
        }

        List<Document> list = pm.getTasks(message.get("peer").toString());

        //log.error(points.toString());
        return Response.ok(new JSONObject().put("tasks",list).toString()).build();

    }

}
