/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.smartsocietyproject.scenario5;

import eu.smartsocietyproject.pf.TaskRequest;
import eu.smartsocietyproject.scenario5.helper.SmartShareTaskDefinition;

import java.util.concurrent.TimeUnit;

public class SmartShareTaskRequest extends TaskRequest {
    
    private int communityTime;
    private TimeUnit communityTimeUnit;
    private int orchestratorTime;
    private TimeUnit orchestratorUnit;

    public SmartShareTaskRequest(SmartShareTaskDefinition definition) {
        super(definition, "GoogleRequestTask");
    }

    @Override
    public String getRequest() {
        //since we know that it is a string node
        return getDefinition().getJson().get("request").asText();
    }

    @Override
    public SmartShareTaskDefinition getDefinition() {
        return (SmartShareTaskDefinition) super.getDefinition();
    }

    public int getCommunityTime() {
        return communityTime;
    }

    public void setCommunityTime(int communityTime) {
        this.communityTime = communityTime;
    }

    public TimeUnit getCommunityTimeUnit() {
        return communityTimeUnit;
    }

    public void setCommunityTimeUnit(TimeUnit communityTimeUnit) {
        this.communityTimeUnit = communityTimeUnit;
    }

    public int getOrchestratorTime() {
        return orchestratorTime;
    }

    public void setOrchestratorTime(int orchestratorTime) {
        this.orchestratorTime = orchestratorTime;
    }

    public TimeUnit getOrchestratorUnit() {
        return orchestratorUnit;
    }

    public void setOrchestratorUnit(TimeUnit orchestratorUnit) {
        this.orchestratorUnit = orchestratorUnit;
    }
    
}
