/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.smartsocietyproject.scenario5.handler;

import com.google.common.collect.ImmutableList;
import eu.smartsocietyproject.peermanager.PeerManagerException;
import eu.smartsocietyproject.pf.*;
import eu.smartsocietyproject.pf.cbthandlers.CBTLifecycleException;
import eu.smartsocietyproject.pf.cbthandlers.CompositionHandler;
import eu.smartsocietyproject.scenario5.SmartShareTaskRequest;
import eu.smartsocietyproject.scenario5.helper.SmartSharePlan;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public class SmartShareCompositionHandler implements CompositionHandler {
    public List<CollectiveWithPlan> compose(ApplicationContext context, 
            ApplicationBasedCollective provisioned, 
            TaskRequest t) throws CBTLifecycleException {
        try {
            //todo-sv: we should probably hide peermanager functions that return
            //residentCollectives into the internal peermanager interface
            //and provide initial queries that target resident collectives
            //but return ABCs
            ResidentCollective rc = context.getPeerManager()
                    .readCollectiveById(provisioned.getId().toString());

            Collection<Member> humans = rc.getMembers().stream()
                    .filter(member -> member.getRole().equals("HumanExpert"))
                    .collect(Collectors.toList());
            
            ApplicationBasedCollective humansCollective = rc.withMembers(humans);
            context.getPeerManager().persistCollective(humansCollective);
            
            SmartSharePlan plan = new SmartSharePlan( humansCollective, (SmartShareTaskRequest)t);
            CollectiveWithPlan cwp = CollectiveWithPlan.of(provisioned, plan);
            return ImmutableList.of(cwp);
        } catch (PeerManagerException ex) {
            throw new CBTLifecycleException(ex);
        } catch (Throwable e) {
            throw new CBTLifecycleException(e);
        }
    }
    
}
