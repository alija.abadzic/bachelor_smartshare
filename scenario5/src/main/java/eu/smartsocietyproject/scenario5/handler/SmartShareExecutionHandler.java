/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.smartsocietyproject.scenario5.handler;

import at.ac.tuwien.dsg.smartcom.callback.NotificationCallback;
import at.ac.tuwien.dsg.smartcom.exception.CommunicationException;
import at.ac.tuwien.dsg.smartcom.model.Identifier;
import at.ac.tuwien.dsg.smartcom.model.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.smartsocietyproject.peermanager.PeerManagerException;
import eu.smartsocietyproject.pf.ApplicationContext;
import eu.smartsocietyproject.pf.AttributeType;
import eu.smartsocietyproject.pf.CollectiveWithPlan;
import eu.smartsocietyproject.pf.TaskResult;
import eu.smartsocietyproject.pf.cbthandlers.CBTLifecycleException;
import eu.smartsocietyproject.pf.cbthandlers.ExecutionHandler;
import eu.smartsocietyproject.pf.helper.InternalPeerManager;
import eu.smartsocietyproject.pf.helper.PeerIntermediary;
import eu.smartsocietyproject.scenario5.helper.SmartSharePlan;
import eu.smartsocietyproject.scenario5.helper.SmartShareTaskResult;
import eu.smartsocietyproject.scenario5.helper.SmartShareUtil;
import eu.smartsocietyproject.smartcom.SmartComServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class SmartShareExecutionHandler implements ExecutionHandler, NotificationCallback {

    private static final Logger log = LoggerFactory.getLogger(SmartShareExecutionHandler.class);


    private  String ANSWER_FROM_PEER = "";


    private SmartSharePlan plan;
    private SmartShareTaskResult result = null;
    private ObjectMapper mapper = new ObjectMapper();
    private String conversationId;
    private String orchestrationId;
    private static InternalPeerManager pm;

    private Lock communityLock;
    private Condition communityCondition;
    private Semaphore communityMaxSemaphore;

    public synchronized TaskResult execute(ApplicationContext context, CollectiveWithPlan agreed) throws CBTLifecycleException {

        this.resetHandler();
        //todo-sv: remove this cast
        SmartComServiceImpl sc = (SmartComServiceImpl) context.getSmartCom();
        pm = (InternalPeerManager) context.getPeerManager();
        Identifier callback = sc.registerNotificationCallback(this);

        try {
            //todo-sv: maybe registration of rest input adapter belongs also here
            //Identifier callback = sc.registerNotificationCallback(this);

            if (!(agreed.getPlan() instanceof SmartSharePlan)) {
                throw new CBTLifecycleException("Wrong plan!");
            }

            plan = (SmartSharePlan) agreed.getPlan();
            ANSWER_FROM_PEER = "<br/> </br> Your request (" + plan.getRequest().getRequest() + ") has been accepted from one of our peers!";

            //this.conversationId = plan.getRequest().getId().toString();
            this.conversationId = callback.getId();
            this.orchestrationId += conversationId;



            String req = "We have new request for you!</br></br>" + plan.getRequest().getRequest();

            //update peer points
            String senderId = plan.getRequest().getDefinition().getSender().getId();

            String url = "localhost:9696/request/accept/" + conversationId;
            Message.MessageBuilder msg = new Message.MessageBuilder()
                    .setType("ask")
                    .setSubtype("request")
                    .setReceiverId(Identifier.collective(plan.getHumans().getId()))
                    //to add real sender id and compare it in output adapter
                    //.setSenderId(Identifier.component("smartShare"))
                    .setSenderId(Identifier.peer(senderId))
                    .setConversationId(conversationId)
                    .setSecurityToken(plan.getRequest().getId().toString())
                    .setContent(req);

            sc.send(msg.create());


            communityLock.lock();
            try {
                //amount of community results we want for orchestrator
                //for demo purposes only set to 1
                communityMaxSemaphore.release(1);

                // this helps us that answer can only be given inside community time and not later
                // semaphore makes this possible
                if (!communityCondition.await(plan.getRequest().getCommunityTime(),
                        plan.getRequest().getCommunityTimeUnit())) {
                    communityMaxSemaphore.drainPermits();
                }
            } finally {
                communityLock.unlock();
            }


            return result;


        } catch (InterruptedException | CommunicationException ex) {
            throw new CBTLifecycleException(ex);
        } finally {
            sc.unregisterNotificationCallback(callback);
        }
    }

    private void resetHandler() {
        this.communityLock = new ReentrantLock();
        this.communityCondition = communityLock.newCondition();
        this.communityMaxSemaphore = new Semaphore(0);

        this.orchestrationId = "smartShare-";
        this.result = new SmartShareTaskResult();
    }

    @Override
    public TaskResult getResultIfQoRGoodEnough() {
        if (this.result.isQoRGoodEnough()) {
            return result;
        }

        return null;
    }

    @Override
    public double resultQoR() {
        return result.QoR();
    }

    @Override
    public void notify(Message message) {
        if (!this.conversationId.equals(message.getConversationId())) {
            return;
        }

        communityLock.lock();
        try {
            //one peer already answered or the community time expired
            if (!communityMaxSemaphore.tryAcquire()) {
                return;
            }

            result.setAnswer(ANSWER_FROM_PEER);

            if (communityMaxSemaphore.availablePermits() == 0) {
                SmartShareUtil.assignPointToPeer(message.getSenderId().getId(),pm);
                pm.updateTaskWithPeer(plan.getRequest().getDefinition().getId().toString(),message.getSenderId().getId());
                communityCondition.signal();
            }
        } finally {
            communityLock.unlock();
        }
    }


}
