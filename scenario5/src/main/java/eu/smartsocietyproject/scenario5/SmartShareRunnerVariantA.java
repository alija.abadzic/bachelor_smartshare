/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.smartsocietyproject.scenario5;

import eu.smartsocietyproject.TaskResponse;
import eu.smartsocietyproject.pf.*;
import eu.smartsocietyproject.pf.helper.InternalPeerManager;
import eu.smartsocietyproject.scenario5.handler.SmartShareExecutionHandler;
import eu.smartsocietyproject.scenario5.helper.SmartShareTaskResult;
import eu.smartsocietyproject.scenario5.helper.SmartShareUtil;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class SmartShareRunnerVariantA extends SmartShareTaskRunner {

    private final  String NO_ANSWER_FROM_PEER = "<br/> <br/>This is feedback on your request: <br/>" + request.getRequest() + " <br/><br/> Unfortunately, none of our peers is available at the moment!";
    
    public SmartShareRunnerVariantA(SmartShareTaskRequest request, SmartSocietyApplicationContext ctx) {
        super(request, ctx);
    }

    @Override
    public TaskResponse call() throws Exception {
        Collective nearbyPeers = this.queryNearbyPeers();
        
        TaskFlowDefinition tfd = this.getDefaultTaskFlowDefinition(nearbyPeers);

        SmartShareTaskResult noAnswerResult = new SmartShareTaskResult();
        noAnswerResult.setAnswer(NO_ANSWER_FROM_PEER);
        
        request.setCommunityTime(1);
        request.setCommunityTimeUnit(TimeUnit.MINUTES);

        //not needed for this scenario
        request.setOrchestratorTime(1);
        request.setOrchestratorUnit(TimeUnit.MINUTES);

        pm.persistTask(request.getDefinition(),request.getDefinition().getSender().getId(),SmartShareUtil.PENDING);

        CollectiveBasedTask cbt = this.createCBT(tfd);
        cbt.start();
        
        TaskResult res = null;
        try{
            // wait one minute for first answer; alija
            res = cbt.get(1, TimeUnit.MINUTES);
        } catch (TimeoutException ex) {
            pm.updateTask(request.getDefinition().getId().toString(),SmartShareUtil.FAILED);
            SmartShareUtil.assignPointToPeer(request.getDefinition().getSender().getId(), (InternalPeerManager) ctx.getPeerManager());
            this.sendResponse(noAnswerResult);
            return TaskResponse.FAIL;
        }
        
        /*if(res == null) {
            // if no result create new collective based task
            cbt = this.createCBT(tfd);
            cbt.start();
            
            res = cbt.get(2, TimeUnit.MINUTES);
        }*/
        
        if(res == null || res.getResult().equals("")) {
            pm.updateTask(request.getDefinition().getId().toString(),SmartShareUtil.FAILED);
            SmartShareUtil.assignPointToPeer(request.getDefinition().getSender().getId(), (InternalPeerManager) ctx.getPeerManager());
            this.sendResponse(noAnswerResult);
            return TaskResponse.FAIL;
        }
        
        this.sendResponse(res);
        pm.updateTask(request.getDefinition().getId().toString(),SmartShareUtil.FINISHED);


        return TaskResponse.OK;
    }
}
