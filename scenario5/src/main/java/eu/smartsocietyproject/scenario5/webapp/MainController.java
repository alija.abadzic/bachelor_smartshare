package eu.smartsocietyproject.scenario5.webapp;


import eu.smartsocietyproject.scenario2.helper.JsonPeer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;

@Controller
public class MainController {

    private final MainService mainService;


    @Autowired
    public MainController(MainService mainService) {
        this.mainService = mainService;
    }

    @RequestMapping("/")
    @GetMapping
    public String index()
    {

        return "login";
    }


    @GetMapping("/home")
    public String home(){
        return "home";
    }

    @PostMapping("/register")
    public String register(){
        return "index";
    }

    @PostMapping("/request")
    public String request(){
        return "index";
    }

}
