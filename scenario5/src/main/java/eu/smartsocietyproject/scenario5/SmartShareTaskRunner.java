/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.smartsocietyproject.scenario5;

import at.ac.tuwien.dsg.smartcom.model.Identifier;
import at.ac.tuwien.dsg.smartcom.model.Message;
import com.fasterxml.jackson.databind.JsonNode;
import eu.smartsocietyproject.peermanager.PeerManagerException;
import eu.smartsocietyproject.peermanager.query.PeerQuery;
import eu.smartsocietyproject.peermanager.query.QueryOperation;
import eu.smartsocietyproject.peermanager.query.QueryRule;
import eu.smartsocietyproject.pf.*;
import eu.smartsocietyproject.pf.helper.InternalPeerManager;
import eu.smartsocietyproject.scenario5.handler.SmartShareCompositionHandler;
import eu.smartsocietyproject.scenario5.handler.SmartShareExecutionHandler;
import eu.smartsocietyproject.scenario5.handler.SmartShareNegotiationHandler;
import eu.smartsocietyproject.scenario5.handler.SmartShareProvisioningHandler;
import eu.smartsocietyproject.smartcom.SmartComServiceImpl;


public abstract class SmartShareTaskRunner implements TaskRunner {

    protected final SmartShareTaskRequest request;
    protected final SmartSocietyApplicationContext ctx;
    protected final InternalPeerManager pm;

    public SmartShareTaskRunner(SmartShareTaskRequest request,
                                SmartSocietyApplicationContext ctx) {
        this.request = request;
        this.ctx = ctx;
        this.pm = (InternalPeerManager) ctx.getPeerManager();
    }

    //todo-sv: what is this for
    @Override
    public JsonNode getStateDescription() {
        return null;
    }
    
    protected Collective queryNearbyPeers() throws PeerManagerException {
        return ApplicationBasedCollective
                .createFromQuery(ctx,
                        PeerQuery.create()
                                .withRule(QueryRule.create("smartShare")
                                        .withValue(AttributeType.from("true"))
                                        .withOperation(QueryOperation.equals)
                                )
                );
    }
    
    protected TaskFlowDefinition getDefaultTaskFlowDefinition(Collective peers) {
        return TaskFlowDefinition
                .onDemandWithOpenCall(new SmartShareProvisioningHandler(),
                        new SmartShareCompositionHandler(),
                        new SmartShareNegotiationHandler(),
                        new SmartShareExecutionHandler())
                .withCollectiveForProvisioning(peers);
    }
    
    protected CollectiveBasedTask createCBT(TaskFlowDefinition tfd) {
        return ctx.registerBuilderForCBTType("smartShare", CBTBuilder.from(tfd)
                        .withTaskRequest(request)).build();
    }
    
    protected void sendResponse(TaskResult res) {
        Message respMsg = new Message.MessageBuilder()
                .setType("smartShare")
                .setSubtype("answer")
                .setContent(res.getResult())
                .setSenderId(Identifier.component("smartShare"))
                .setConversationId("smartShare")
                .setReceiverId(this.request.getDefinition().getSender())
                .create();
        
        try {
        //todo-sv: fix interface and remove cast
        ((SmartComServiceImpl) ctx.getSmartCom()).send(respMsg);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
