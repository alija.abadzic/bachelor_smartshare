package eu.smartsocietyproject.scenario5;

import com.typesafe.config.Config;
import eu.smartsocietyproject.pf.*;
import eu.smartsocietyproject.scenario5.helper.SmartShareTaskDefinition;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Set;


public class SmartShareApplication extends Application {


    private SmartSocietyApplicationContext ctx;

    public SmartShareApplication(SmartSocietyApplicationContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public String getApplicationId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void init(ApplicationContext context, Config config) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<CollectiveKind> listCollectiveKinds() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TaskRequest createTaskRequest(TaskDefinition definition) {
        if(definition instanceof SmartShareTaskDefinition) {
            return new SmartShareTaskRequest((SmartShareTaskDefinition)definition);
        }
        return null;
    }

    @Override
    public TaskRunner createTaskRunner(TaskRequest request) {
        if(request instanceof SmartShareTaskRequest) {
            if(((SmartShareTaskDefinition)request.getDefinition()).isVariantA()) {
                return new SmartShareRunnerVariantA((SmartShareTaskRequest)request, ctx);
            }
        }
        throw new UnsupportedOperationException("Not supported request type!");
    }

    public SmartSocietyApplicationContext getCtx() {
        return ctx;
    }
}
