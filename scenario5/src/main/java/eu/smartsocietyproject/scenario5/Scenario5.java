package eu.smartsocietyproject.scenario5;

import at.ac.tuwien.dsg.smartcom.callback.NotificationCallback;
import at.ac.tuwien.dsg.smartcom.exception.CommunicationException;
import at.ac.tuwien.dsg.smartcom.model.Identifier;
import at.ac.tuwien.dsg.smartcom.model.Message;
import at.ac.tuwien.dsg.smartcom.model.PeerChannelAddress;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.MongoClient;
import eu.smartsocietyproject.peermanager.PeerManagerException;
import eu.smartsocietyproject.pf.*;
import eu.smartsocietyproject.pf.helper.InternalPeerManager;
import eu.smartsocietyproject.pf.helper.PeerIntermediary;
import eu.smartsocietyproject.runtime.Runtime;
import eu.smartsocietyproject.scenario2.helper.GreenMail.RunMailServer;
import eu.smartsocietyproject.scenario2.helper.GreenMailOutputAdapter;
import eu.smartsocietyproject.scenario2.helper.JsonPeer;
import eu.smartsocietyproject.scenario2.helper.PeerLoader;
import eu.smartsocietyproject.scenario5.helper.RESTInputAdapter.CustomRESTInputAdapter;
import eu.smartsocietyproject.scenario5.helper.SmartShareTaskDefinition;
import eu.smartsocietyproject.smartcom.PeerChannelAddressAdapter;
import eu.smartsocietyproject.smartcom.SmartComServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;
public class Scenario5 implements NotificationCallback {

    private static Runtime runtime;
    private static final boolean DEMO = true;
    private static final boolean variantA = true;
    private static Logger logger = LoggerFactory.getLogger(Scenario5.class);
    private static InternalPeerManager pm;
    private static ObjectMapper mapper = new ObjectMapper();




    public static void main(String[] args) throws IOException, CommunicationException {

        if (DEMO) {
            RunMailServer.start();
        }

        CollectiveKindRegistry kindRegistry = CollectiveKindRegistry
                .builder().register(CollectiveKind.EMPTY).build();


        MongoRunner runner = MongoRunner.withPort(6666);

        PeerManagerMongoProxy.Factory pmFactory
                = PeerManagerMongoProxy.factory(runner.getMongoDb());

        MongoClient client = new MongoClient("localhost", 6666);

        SmartSocietyApplicationContext context
                = new SmartSocietyApplicationContext(kindRegistry,
                pmFactory,
                new SmartComServiceImpl.Factory(client));


        SmartComServiceImpl smartCom = (SmartComServiceImpl) context.getSmartCom();


        if (DEMO) {
            smartCom.getCommunication()
                    .removeOutputAdapter(Identifier.adapter("Email"));
            smartCom.getCommunication()
                    .registerOutputAdapter(GreenMailOutputAdapter.class);
        }


        PeerLoader.laodPeers(context);

        smartCom.registerNotificationCallback(new Scenario5());
        Properties props = new Properties();
        props.load(Scenario5.class.getClassLoader()
                .getResourceAsStream("EmailAdapter.properties"));
        //maybe not needed in this scenario
        smartCom.addEmailPullAdapter("smartShare", props);
        smartCom.getCommunication().addPushAdapter(new CustomRESTInputAdapter(9696, "request",context));

        pm = (InternalPeerManager) context.getPeerManager();

        Scenario5.runtime = new Runtime(context, new SmartShareApplication(context));

        Scenario5.runtime.run();

    }

    @Override
    public void notify(Message message) {
        if (message.getConversationId().equals("smartShare")) {
            switch (message.getType()) {
                case "request":
                    ObjectNode rqa = JsonNodeFactory.instance.objectNode();
                    rqa.set("request", JsonNodeFactory.instance
                            .textNode(message.getContent().trim()));
                    Scenario5.runtime.startTask(new SmartShareTaskDefinition(rqa,
                            message.getSenderId(), variantA));
                    break;
                case "register":
                    createPeer(message.getContent());
                    break;
                default:
                    logger.error("Unknown request type: " + message.getType());
            }

        }
    }


    private void createPeer(JsonPeer jsonPeer) {
        pm.persistPeer(PeerIntermediary
                .builder(jsonPeer.getName(),"HumanExpert")
                .addDeliveryAddress(PeerChannelAddressAdapter
                        .convert(new PeerChannelAddress(
                                Identifier.peer(jsonPeer.getName()),
                                Identifier.channelType("Email"),
                                Arrays.asList(jsonPeer.getChannel()))
                        )
                )
                .addAttribute("smartShare", AttributeType.from("true"))
                .addAttribute("password", AttributeType.from(jsonPeer.getPw()))
                .addAttribute("points",AttributeType.from(10))
                .addAttribute("uniqueId",AttributeType.from(UUID.randomUUID().toString()))
                .build());
    }

    private void createPeer(String messageContent) {
        try {
            JsonNode node = mapper.readTree(messageContent);
            Optional<String> name =
                    Optional.ofNullable(node.get("name")).map(JsonNode::asText);
            Optional<String> channel =
                    Optional.ofNullable(node.get("channel")).map(JsonNode::asText);
            Optional<String> password =
                    Optional.ofNullable(node.get("password")).map(JsonNode::asText);
            JsonPeer jsonPeer = new JsonPeer();
            jsonPeer.setChannel(channel.get());
            jsonPeer.setName(name.get());
            jsonPeer.setPw(password.get());
            createPeer(jsonPeer);
            RunMailServer.addPeer(jsonPeer);
            logger.info(String.format("Created User %s ", name.get()));
        } catch (IOException e) {
            logger.warn(String.format("Request with wrong format: %s", messageContent));
        }
    }


    public static Integer loginPeer(String messageContent){
        try {
            JsonNode node = mapper.readTree(messageContent);
            Optional<String> name =
                    Optional.ofNullable(node.get("name")).map(JsonNode::asText);
            Optional<String> password =
                    Optional.ofNullable(node.get("password")).map(JsonNode::asText);
            JsonPeer jsonPeer = new JsonPeer();
            jsonPeer.setName(name.get());
            jsonPeer.setPw(password.get());
            logger.info(String.format("User %s trying to log in ", name.get()));
            PeerIntermediary peerIntermediary;
            try {
                peerIntermediary = pm.readPeerById(name.get());
            } catch (PeerManagerException e) {
                return null;
            }
            try {
                String realPW = (String) peerIntermediary.getAttribute("password",AttributeType.STRING).getValue();
                Integer points = (Integer) peerIntermediary.getAttribute("points",AttributeType.INTEGER).getValue();
                if(realPW.equals(password.get())){
                    return points;
                }
                else {
                    return null;
                }
            } catch (PeerManagerException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            logger.warn(String.format("Request with wrong format: %s", messageContent));
        }
        return null;
    }
}
